﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetFwTypeLib;

namespace FireWallHelper
{
    public enum ActionStatus
    {
        Error = 0,
        InvalidArgs = 1,
        Done = 2,
        Existed = 3
    }

    public enum ActionTypes
    {        
        Program,
        Port
    }

    public enum PortProtocols
    {
        UDP,
        TCP
    }

    public enum RuleDirections
    {
        In,
        Out,
        Max
    }

    /*
    Arguments
    Port "RuleName" TCP In True
    Port "RuleName" UDP Out False
    Program "RuleName" "Path" In True
    Program "RuleName" "Path" Out False
    */

    class Program
    {
        static void Main(string[] args)
        {            
            ActionStatus status = ActionStatus.InvalidArgs;
            ActionTypes type = ActionTypes.Port;

            try
            {
                type = (ActionTypes)Enum.Parse(typeof(ActionTypes), args[0]);

                switch (type)
                {
                    case ActionTypes.Program:
                        try
                        {
                            string name = args[1];
                            string path = args[2];                            
                            RuleDirections direction = (RuleDirections)Enum.Parse(typeof(RuleDirections), args[3]);
                            bool isEnabled = bool.Parse(args[4]);
                            status = Do_Program(name, path, direction, isEnabled);
                        }
                        catch (Exception)
                        {
                            status = ActionStatus.InvalidArgs;
                        }
                        break;

                    case ActionTypes.Port:                       
                    default:
                        try
                        {
                            string name = args[1];
                            string port = args[2];
                            PortProtocols protocal = (PortProtocols)Enum.Parse(typeof(PortProtocols), args[3]);
                            RuleDirections directory = (RuleDirections)Enum.Parse(typeof(RuleDirections), args[4]);
                            bool isEnabled = bool.Parse(args[5]);
                            status = Do_Port(name, port, protocal, directory, isEnabled);
                        }
                        catch (Exception)
                        {
                            status = ActionStatus.InvalidArgs;
                        }
                        break;
                }
            }
            catch (Exception)
            {
                status = ActionStatus.Error;               
            }

            switch (status)
            {
                case ActionStatus.Error:
                    Console.WriteLine("Error");
                    break;

                case ActionStatus.InvalidArgs:
                    Console.WriteLine("Invalid Args");
                    break;

                case ActionStatus.Done:
                    Console.WriteLine("Done");
                    break;

                case ActionStatus.Existed:
                    Console.WriteLine("Rule is existed. Parameter is updated");
                    break;

                default:
                    Console.WriteLine("Invalid status");
                    break;
            }            
            Environment.Exit((int)status);
        }

        #region Private Method
        /// <summary>
        /// Do Program Operation
        /// </summary>
        /// <param name="name">Rule name</param>
        /// <param name="path">Software path</param>
        /// <param name="action">allow action</param>
        /// <param name="direction">allow direction</param>
        /// <param name="isEnabled">is rule enable</param>
        /// <returns></returns>
        private static ActionStatus Do_Program(string name, string path, RuleDirections direction, bool isEnabled)
        {
            ActionStatus status = ActionStatus.Error;
            
            try
            {
                Type type = Type.GetTypeFromProgID("HNetCfg.FwPolicy2");
                INetFwPolicy2 policy = (INetFwPolicy2)Activator.CreateInstance(type);
                INetFwRule2 rule = null;
                if (isEnabled)
                {
                    //Check and enable if Rule is available
                    if (CheckRule(policy, name, out rule))
                    {
                        rule.Enabled = true;
                        status = ActionStatus.Existed;
                    }
                    else
                    {
                        //Property Order has to be correct
                        rule = (INetFwRule2)Activator.CreateInstance(Type.GetTypeFromProgID("HNetCfg.FWRule"));
                        rule.Enabled = true;
                        rule.Action = NET_FW_ACTION_.NET_FW_ACTION_ALLOW;                        
                        rule.ApplicationName = path;                        
                        switch (direction)
                        {
                            case RuleDirections.In:
                                rule.Direction = NET_FW_RULE_DIRECTION_.NET_FW_RULE_DIR_IN;
                                break;

                            case RuleDirections.Out:
                                rule.Direction = NET_FW_RULE_DIRECTION_.NET_FW_RULE_DIR_OUT;
                                break;

                            case RuleDirections.Max:
                            default:
                                rule.Direction = NET_FW_RULE_DIRECTION_.NET_FW_RULE_DIR_MAX;
                                break;
                        }
                        rule.InterfaceTypes = "All";
                        rule.Name = name;
                        policy.Rules.Add(rule);
                        status = ActionStatus.Done;
                    }
                }
                else if(CheckRule(policy, name, out rule))
                {
                    rule.Enabled = false;
                    status = ActionStatus.Done;
                }
            }
            catch (Exception)
            {
                status = ActionStatus.Error;    
            }
            return status;
        }

        /// <summary>
        /// Do Port operation
        /// </summary>
        /// <param name="name">Rule name</param>
        /// <param name="port">Local Port</param>
        /// <param name="protocal">Internet Protocal</param>
        /// <param name="direction">allow direction</param>
        /// <param name="isEnabled">is rule enable</param>
        /// <returns></returns>
        private static ActionStatus Do_Port(string name, string port, PortProtocols protocal, RuleDirections direction, bool isEnabled)
        {
            ActionStatus status = ActionStatus.Error;
            try
            {
                Type type = Type.GetTypeFromProgID("HNetCfg.FwPolicy2");
                INetFwPolicy2 policy = (INetFwPolicy2)Activator.CreateInstance(type);
                INetFwRule2 rule = null;

                if (isEnabled)
                {
                    //Check and enable if Rule is available
                    if (CheckRule(policy, name, out rule))
                    {                        
                        rule.Enabled = true;

                        //Update Port
                        var portText = port.ToString();
                        if (rule.LocalPorts != portText)
                        {
                            rule.LocalPorts = portText;
                        }                                            
                        status = ActionStatus.Existed;
                    }
                    //Create and enable if Rule is no available
                    else
                    {
                        //Property Order has to be correct
                        rule = (INetFwRule2)Activator.CreateInstance(Type.GetTypeFromProgID("HNetCfg.FWRule"));
                        rule.Enabled = true;
                        rule.Action = NET_FW_ACTION_.NET_FW_ACTION_ALLOW;
                        switch (protocal)
                        {
                            case PortProtocols.UDP:
                                rule.Protocol = (int)NET_FW_IP_PROTOCOL_.NET_FW_IP_PROTOCOL_UDP;
                                break;

                            case PortProtocols.TCP:
                            default:
                                rule.Protocol = (int)NET_FW_IP_PROTOCOL_.NET_FW_IP_PROTOCOL_TCP;
                                break;
                        }                        
                        switch (direction)
                        {
                            case RuleDirections.In:
                                rule.Direction = NET_FW_RULE_DIRECTION_.NET_FW_RULE_DIR_IN;
                                break;

                            case RuleDirections.Out:
                                rule.Direction = NET_FW_RULE_DIRECTION_.NET_FW_RULE_DIR_OUT;
                                break;

                            case RuleDirections.Max:
                            default:
                                rule.Direction = NET_FW_RULE_DIRECTION_.NET_FW_RULE_DIR_MAX;
                                break;
                        }
                        rule.LocalPorts = port;
                        rule.Name = name;
                        rule.Profiles = policy.CurrentProfileTypes;
                        policy.Rules.Add(rule);
                        status = ActionStatus.Done;
                    }              
                }
                //Disable if Rule is available
                else if (CheckRule(policy, name, out rule))
                {
                    rule.Enabled = false;
                    status = ActionStatus.Done;
                }
            }
            catch (Exception)
            {
                status = ActionStatus.Error;
            }
            return status;
        }

        /// <summary>
        /// Check if Rule is available
        /// </summary>
        /// <param name="policy">Fireware Polica</param>
        /// <param name="name">rule Name</param>
        /// <param name="rule">out Rule</param>
        /// <returns></returns>
        private static bool CheckRule(INetFwPolicy2 policy, string name, out INetFwRule2 rule)
        {
            bool hasRule = false;
            rule = null;

            foreach (var item in policy.Rules)
            {
                var tmp = item as INetFwRule2;
                if (tmp != null && tmp.Name == name)
                {
                    hasRule = true;
                    rule = tmp;
                    break;
                }
            }

            return hasRule;
        }
        #endregion
    }
}
